<?php
    header("Content-type:Application-json");
    header("Access-Control-Allow-Origin: *");
    $nama = array("Wira", "Aditya", "Surya", "Kencana");
    $alamat = array("Merdeka", "Ayani", "Kartini", "Sulawesi", "Gunung Agung", "Gatot subroto");
    $data = array();
    for($x=0; $x<20; $x++){
        $row = array(
            "nama"=>$nama[array_rand($nama,1)]." ".$nama[array_rand($nama,1)],
            "alamat"=>"Jalan ".$alamat[array_rand($alamat,1)]." Nomor ".rand(1,100)
        );
        array_push($data,$row);
    }
    print_r(json_encode($data));
?>