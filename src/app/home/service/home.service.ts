import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../home.interface';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private http: HttpClient) { }
  getUserdata() {
    return this.http.get<User[]>('http://localhost/ionicInfiniteScroll/api/loadData.php');
  }
}
