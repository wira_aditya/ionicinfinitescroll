import { Component, ViewChild } from '@angular/core';
import {HomeState} from './state/home.state';

import {Select, Store} from '@ngxs/store';
import {User} from './home.interface';


import {Observable} from 'rxjs';
import {AddUser, GetUSer} from './state/home.action';

import {IonInfiniteScroll} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
  @Select(HomeState.getTodoList) todos: Observable<User[]>;
  public total;
  constructor(private store: Store) {
    this.addData();
    this.todos.subscribe(async (res) => {
      this.total = res.length;
      console.log(res.length);
    });
  }

  async addData() {
    await this.store.dispatch(new AddUser());
  }
  loadData(event) {
    console.log(this.total);

    setTimeout(() => {
        this.addData();
        event.target.complete();
        if (this.total === 100) {
          event.target.disabled = true;
        }
    }, 500);
  }
  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}
