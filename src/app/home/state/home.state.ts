import { StateContext, Selector, State, Action} from '@ngxs/store';
import { patch, append} from '@ngxs/store/operators';

import { ListUser} from '../home.interface';
import {AddUser} from './home.action';
import {HomeService} from '../service/home.service';
import {tap} from 'rxjs/operators';
@State<ListUser>({
    name: 'users',
    defaults: {
        users: []
    }
})
export class HomeState {
    constructor(private service: HomeService) {

    }
    @Selector()
    static getTodoList(state: ListUser) {
        return state.users;
    }
    // static getLength(state: ListUser) {
    //     return state.users.length;
    // }
    @Action(AddUser)
    AddUser(ctx: StateContext<ListUser>, action: AddUser) {
        return this.service.getUserdata().pipe(tap((res) => {
            ctx.setState(
                patch({
                  users: append(res)
                })
            );
        }));
    }

}
